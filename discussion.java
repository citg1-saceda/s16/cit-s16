// [SECTION] Some Test Work Flow

// Smoke test for posts routes

===============
Creating posts:
===============
1. Send a POST request via postman to generate a JWT token with the user details sent at http://localhost:8080/authenticate.
2. Sened a POST request via postman to test if we can create at http://localhost:8080/post.
3. Add the JWT token in the header of the request as "Authorization".
4. Send the data as a raw JSON format using postman.
5. Include the request body data to be sent along with the request:
    {
        "title": "Hello World",
        "content": "This is another post"
    }
6. Click on "send" to process the postman request.
7. The API should respond with a message of "Post created successfully."

=====================
Retrieving all posts:
=====================
1. Send a GET request via postman to test if we can retrieve all posts at http://localhost:8080/posts.
2. Click  on "send" to process the postman request.
3. The API should respond with a message of an array which includes all posts.

================
Updating a post:
================
1. Send a POST request via postman to generate a JWT token with the user details sent at http://localhost:8080/authenticate.
2. Send a PUT request via postman to test if we can update a specific post at http://localhost:8080/posts/<postId>.
3. Add the JWT token in the header of the request as "Authorization".
4. Send the data as a raw JSON format using postman.
5. Include the request body data to be sent along with the request:
    {
        "title": "Hello again!",
        "content": "This post has been updated!"
    }
6. Click on "send" to process the postman request.
// 7. The API should respond with a message of "Post updated successfully." 
7. The API should respond with an object of the updated post.


================
Deleting a post:
================
1. Send a POST request via postman to generate a JWT token with the user details sent at http://localhost:8080/authenticate.
2. Send a DELETE request via postman to test if we can delete a specific post at http://localhost:8080/posts/<postId>.
3. Add the JWT token in the header of the request as "Authorization".
4. Click on "send" to process the postman request.
// 5. The API should respond with a message of "Post deleted successfully." 
5. The API should respond with an object of the deleted post.

// Mini Activity
============================
Retrieving all user's posts:
============================
1. Send a POST request via postman to generate a JWT token with the user details sent at http://localhost:8080/authenticate.
2. Send a GET request via postman to test if we can retrieve all posts at http://localhost:8080/myPosts.
4. Add the JWT token in the header of the request as "Authorization".
5. Click  on "send" to process the postman request.
6. The API should respond with a message of an array which includes all of the specific user's posts.

// [SECTION] AWS Deployment

/*
1.   Logging in to IAM Account
  	- login using the IAM account provided to you.
  	- https://436816677911.signin.aws.amazon.com/console
  	- You will be prompt to change the temporary password, make sure that it follows the following:
  		- Atleast one uppercase letter
  		- Atleast one numeric character
*/

/*
2. Selecting a Region
	- Services are tied to a selected region. Before proceeding, select the Oregon (us-west-2) region.
*/

/*
3. Launch an EC2 Instance
	- Go to the EC2 Management Console and select the Launch Instance button.
		- You may provide this link: https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#Home:
		- or click EC2 instance
		- Alternatively, you can go directly to the Launch Instance Wizard: https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#LaunchInstances
*/

/*
4.  Creeating an EC2 instance

  Step 1: Add an instance name
    - Format: bucket-batch-class_num-lastName-instance (class_num as the class number, e.g cit01)

  Step 2: Choose an Amazon Machine Image (AMI)
    - Select the "Ubuntu Server", the latest version of the server is at Ubuntu 22.04

  Step 3: Choose an Instance Type
    - Select the t2.micro instance type which is eligible for free tier usage. (which is selected by default)

  Step 4:	Key Pair Configuration
    - Create a new "key-pair" which will be used to connect to your instance.
    - "Key pair name" should be similar name with your instance (bucket-batch-class_num-lastName-instance)
    - "Key pair type" is RSA
    - "Private key file format" is .pem
        - PEM means "Privacy Enhanced Mail" and is a file that contains cryptographic keys for securely connecting to our EC2 instance.
    - Click "Create key pair".

  Step 5: Network setting
    - Under "Firewall(security group)", configure the following:
      - Choose "Create Security Group"
      - Check "Allow SSH traffic from Anywhere / 0.0.0.0"
      - Check "Allow HTTPS traffic from the internet"
      - Check "Allow HTTP traffic from the internet"

  Step 6: Review Instance Launch
    - Once the settings for the new instance have been verified, click the "Launch button".
    - After launching the instance a .pem file will be generated and downloaded. Transfer this in your ".ssh folder".
    - To locate your ".ssh/" folder, open a gitbash terminal and type "cd .ssh/" to change the directory.
    - Once you're in the ".ssh/" folder directory, type in the terminal the "explorer ." to open the file explorer.

  Step 7: Click the instance link
    - This show more details about the created instance.
*/

